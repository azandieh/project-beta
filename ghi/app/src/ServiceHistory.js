import React, {useEffect, useState} from "react";

export default function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [search, setSearch] = useState('');
  const [autos, setAutos] = useState([]);


  async function LoadAutos() {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
        const data = await response.json();
        setAutos(data.autos)
    } else {
        console.error('Error loading automobiles:', response.statusText);
    }
}


async function LoadAppointments() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
    } else {
        console.error('Error loading appointments:', response.statusText);
    }
}

  const handleSearch = (event) => {
      const value = event.target.value;
      setSearch(value.toLowerCase());
      };

const searchAppointments = (appointments, search) => {
    let term = [];
    for (let appt of appointments) {
      if (appt['vin'].toLowerCase().includes(search)) {
        term.push(appt)
      }
    }
    return term
};

const filtered = searchAppointments(appointments, search)

useEffect(() => {
    LoadAppointments();
    LoadAutos();
}, []);


    return (
      <div>
      <div className="ui search">
        <div className="ui icon input">
          <input onChange={handleSearch} value={search} placeholder="Search by VIN..." name="search" id="search" className="form-control" />
        </div>
        <button className="btn btn-primary">submit</button>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Reason</th>
            <th>Technician</th>
          </tr>
        </thead>
        <tbody>
          {filtered.map((appointment) => {
            const automobile = autos.find(auto => auto.vin === appointment.vin);
            const isVIP = automobile ? "Yes" : "No";
            return (
              <tr key={appointment.id}>
                <td>{ appointment.vin }</td>
                <td> { isVIP } </td>
                <td>{ appointment.customer }</td>
                <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                <td>{ appointment.reason }</td>
                <td> {appointment.technician.first_name} {appointment.technician.last_name} </td>
                <td className="w-25">
              </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
    );
  }
