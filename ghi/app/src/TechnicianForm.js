import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function TechnicianForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const navigate = useNavigate();


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const techURL = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(techURL, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      console.log(newTechnician);
      setFirstName("");
      setLastName("");
      setEmployeeId("");
      navigate('/technicians')
    } else {
      console.error('Error creating technician:', response.statusText);
  }
  };



  return (
    <div>
      <div className="my-5 container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician 🧑‍🏭</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input onChange={(event) => setFirstName(event.target.value)} value={firstName} type="text" id="first_name" placeholder="first_name" className="form-control"/>
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={(event) => setLastName(event.target.value)} value={lastName} type="text" id="last_name" placeholder="Last Name" className="form-control"/>
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={(event) => setEmployeeId(event.target.value)} value={employeeId} type="text" id="employee_id" placeholder="Employee Id" className="form-control"/>
                  <label htmlFor="employee_id">Employee Id</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
