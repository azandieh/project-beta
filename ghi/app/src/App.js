import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonHistory from './SalesPersonHistory';
import SaleForm from './SaleForm';
import SaleList from './SaleList';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelsList from './VehicleModelList';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={< ManufacturerList manufacturers={props.manufacturers} />} />
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="customers">
            <Route index element={< CustomerList customers={props.customers} />} />
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="automobiles">
              <Route path="" element={<AutomobileList automobiles={props.automobiles}/>}/>
              <Route path="new" element={<AutomobileForm models={props.vehicleModels} />}/>
            </Route>

          <Route path="models">
              <Route index element={< VehicleModelsList models={props.models} />} />
              <Route path="new" element={<VehicleModelForm/>} />
          </Route>
          <Route path="sales">
              <Route index element={< SaleList models={props.sales} />} />
              <Route path="new" element={<SaleForm/>} />
          </Route>
          <Route path="salespeople">
            <Route index element={< SalesPersonHistory models={props.sales} />} />
            <Route path="new" element={<SalesPersonForm/>} />
          </Route>

          <Route path="technicians">
            <Route index element={<TechnicianList technicians={props.technicians} />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList appointments={props.appointments} />} />
            <Route path="create" element={<AppointmentForm tech={props.tech}/>} />
          </Route>
          <Route path="servicehistory">
            <Route index element={<ServiceHistory appointments={props.appointments} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
