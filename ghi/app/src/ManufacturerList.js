import { Link } from 'react-router-dom';
import { React, useState, useEffect } from 'react';


function ManufacturerList(props) {
    const [manufacturer, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);



    return (
        <div>
            <br />
            <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <Link to='/manufacturers/new' className='btn btn-primary btn-lg px-4 gap-3'>Click here to create a Manufacturer</Link>
            </div>
            <br />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturer.map(manufacturer => {
                        return (
                            <tr key={ manufacturer.id }>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ManufacturerList;
