import { Link } from 'react-router-dom';
import { React, useState, useEffect } from 'react';


function CustomerList(props) {
    const [customer, setCustomers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        fetchData();
        }, []);



    return (
        <div>
            <br />
            <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <Link to='/customers/new' className='btn btn-primary btn-lg px-4 gap-3'>Click here to create a Customer</Link>
            </div>
            <br />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map(customer => {
                        return (
                            <tr key={ customer.id }>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.address }</td>
                                <td>{ customer.phone_number }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default CustomerList;
