import React, { useState, useEffect }  from "react";

export default function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    async function LoadAppointments() {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        } else {
            console.error('Error loading appointments:', response.statusText);
        }
    }

    async function LoadAutos() {
        const response = await fetch("http://localhost:8100/api/automobiles/");
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        } else {
            console.error('Error loading autos:', response.statusText);
        }
    }
    useEffect(() => {
        LoadAppointments();
        LoadAutos();
    }, []);

    async function handleCancel(id) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({ status: 'Cancelled' }),
            });
            if (response.ok) {
              setAppointments(appointments.filter(appointment => appointment.id !== id));
            } else {
              console.error('Error canceling appointment:', response.statusText);
            }
          } catch (error) {
            console.error('Error canceling appointment:', error);
          }
    }

    async function handleFinish(id) {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({ status: 'Finished' }),
            });

            if (response.ok) {
              setAppointments(appointments.filter(appointment => appointment.id !== id));
            } else {
              console.error('Error finishing appointment:', response.statusText);
            }
          } catch (error) {
            console.error('Error finishing appointment:', error);
          }
    }


    return (
        <div>
            <h1>Appointments 📖</h1>
            {appointments && appointments.length > 0 ? (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => {
                            const automobile = autos.find(auto => auto.vin === appointment.vin);
                            const isVIP = automobile ? "Yes" : "No";
                            return (
                                <tr key={appointment.id}>
                                    <td>{ appointment.vin }</td>
                                    <td>{ isVIP }</td>
                                    <td>{ appointment.customer }</td>
                                    <td>
                                        {new Date(appointment.date_time).toLocaleString('en-US', {
                                            month: "2-digit",
                                            day: "2-digit",
                                            year: "2-digit",
                                        })}
                                    </td>
                                    <td>{new Date(appointment.date_time).toLocaleString('en-US', {
                                            hour: "2-digit",
                                            minute: "numeric",
                                            hour12: true,
                                        })}</td>
                                    <td>{ appointment.technician.first_name} { appointment.technician.last_name } { appointment.technician.employee_id }</td>
                                    <td>{ appointment.reason }</td>
                                    <td className="text-center">
                                        <button className="btn btn-danger" onClick={() => handleCancel(appointment.id)}> Cancel </button>
                                        <button className="btn btn-success" onClick={() => handleFinish(appointment.id)}> Finish </button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            ) : (
                <p>No appointments scheduled.</p>
            )}
            <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <a href='/appointments/create' className='btn btn-primary btn-lg px-4 gap-3'>Create Appointment</a>
            </div>
        </div>
    );
}
