import React, { useState, useEffect }  from "react";

export default function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    async function LoadTechnicians() {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        } else {
            console.error('Error loading technicians:', response.statusText);
        }
    }

    useEffect(() => {
        LoadTechnicians();
    }, []);


    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map((tech) => {
                        return (
                            <tr key={ tech.id }>
                                <td>{ tech.first_name }</td>
                                <td>{ tech.last_name }</td>
                                <td>{ tech.employee_id }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div className='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <a href='/technicians/new' className='btn btn-primary btn-lg px-4 gap-3'>Create Technician</a>
            </div>
        </div>
    );
}
