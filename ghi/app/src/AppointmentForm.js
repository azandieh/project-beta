import React, {useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';


export default function NewAppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [customer, setCustomer] = useState('');
    const [vin, setVin] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const navigate = useNavigate();


    const fetchData = async () =>{
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchData()
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault()
        const datetime = new Date(date + " " + time).toISOString();

        const data = {};
            data.date_time = datetime;
            data.reason = reason;
            data.customer = customer;
            data.technician = technician;
            data.vin = vin;

        const apptUrl = 'http://localhost:8080/api/appointments/create/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(apptUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician)
            setCustomer('');
            setVin('');
            setDate('');
            setTime('');
            setReason('');
            setTechnicians('');
            navigate('/appointments');
        } else {
            console.error('Error submitting:', response.statusText);
        }
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="add-auto-form">
                        <div className="form-floating mb-3">
                            <h6> Automobile VIN </h6>
                            <input onChange={(event) => setVin(event.target.value)} value={vin} type="text" id="vin" className="form-control"/>
                        </div>
                        <div className="form-floating mb-3">
                            <h6> Customer </h6>
                            <input onChange={(event) => setCustomer(event.target.value)} value={customer} type="text" id="customer" className="form-control"/>
                        </div>
                        <div className="form-floating mb-3">
                            <h6> Date </h6>
                            <input onChange={(event) => setDate(event.target.value)} value={date} placeholder="date" type="date" name="date" id="date" className="form-control" />
                        </div>
                        <div className="form-floating mb-3">
                            <h6> Time </h6>
                            <input onChange={(event) => setTime(event.target.value)} value={time} placeholder="time" type="time" name="time" id="time" className="form-control" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="name" className="form-label">Technician</label>
                            <select className="form-select" value={technician} onChange={(event) => setTechnician(event.target.value)}>
                            <option value="">Choose a technician...</option>
                            {technicians?.map((tech) => {
                            return (
                            <option key={tech.id} value={tech.id}>
                            {tech.first_name} {tech.last_name} ({tech.employee_id})
                            </option>
                            )})}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <h6> Reason </h6>
                            <input onChange={(event) => setReason(event.target.value)} value={reason} type="text" id="reason" className="form-control"/>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
