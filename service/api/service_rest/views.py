from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href"
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
            status = 200
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
                status = 200
            )
        except:
            response = JsonResponse(
                {"message": "Could not create technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def detail_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
                status = 200
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            tech = Technician.objects.get(id=pk)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
                status = 200
                )
        except Technician.DoesNotExist:
                response = JsonResponse({"message": "Does not exist"})
                response.status_code = 404
                return response


@require_http_methods(["GET"])
def list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentEncoder,
                safe=False,
                status=200
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["POST"])
def create_appointment(request):
    try:
        content = json.loads(request.body)
        technician_id = content["technician"]
        technician = Technician.objects.get(id=technician_id)
        content["technician"] = technician

    except Technician.DoesNotExist:
        response = JsonResponse(
        {"message": "Does not exist"}
        )
        response.status_code = 400
        return response

    appointments = Appointment.objects.create(**content)
    return JsonResponse(
        appointments,
        encoder=AppointmentEncoder,
        safe=False,
        status=201
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def detail_appointments(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                status=200
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=pk).update(**content)
            appt = Appointment.objects.get(id=pk)
            appointment.status = "Created"
            appointment.save()
            return JsonResponse(
                appt,
                encoder=Appointment,
                safe=False,
                status=201
                )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "Cancelled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                status=201
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Could not find appointment"}, status=404)
    else:
        return JsonResponse({"message": "Bad request"}, status=400)

@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "Finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                status=201
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Could not find appointment"}, status=404)
    else:
        return JsonResponse({"message": "Bad request"}, status=400)
