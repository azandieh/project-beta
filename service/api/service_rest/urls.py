from django.urls import path
from .views import list_technician, detail_technician, list_appointments, create_appointment, detail_appointments, cancel_appointment, finish_appointment

urlpatterns = [
    path("technicians/", list_technician, name="list_technician"),
    path("technicians/<int:pk>/", detail_technician, name="detail_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/create/", create_appointment, name="create_appointment"),
    path("appointments/<int:pk>/", detail_appointments, name="detail_appointments"),
    path("appointments/<int:pk>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", finish_appointment, name="finish_appointment")
]
