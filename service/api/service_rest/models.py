from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, null=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=40, null=True)
    last_name = models.CharField(max_length=40, null=True)
    employee_id = models.CharField(max_length=40, null=True)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(Technician,
                                on_delete=models.CASCADE,
                                related_name='appointments')
