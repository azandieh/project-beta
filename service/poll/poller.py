import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
from service_rest.models import AutomobileVO
# from service_rest.models import Something

def get_vin():
    url = 'http://project-beta-inventory-api-1:8000/api/automobiles'
    response = requests.get(url)
    content = json.loads(response.content)
    for v in content["autos"]:
        AutomobileVO.objects.update_or_create(
            import_href=v["href"],
            defaults={
                "vin": v["vin"]
            }
        )
    print(content)


def poll():
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            get_vin()
        except Exception as e:
            print("not working")
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
