# CarCar

Team:

* Aurash Zandieh - Sales Microservice
* Melody Oberg - Service Microservice

## How to Start the Project
1. Open up terminal
2. Git clone: https://gitlab.com/azandieh/project-beta.git
3. cd into project-beta
3. docker volume create beta-data
4. docker-compose build
5. docker-compose up
6. Visit http://localhost:3000 on browser

## Service microservice

Explain your models and integration with the inventory
microservice, here.

I created 3 models: Technician, Appointment, & AutomobileVO. The technician model holds information about a technician's first and last name, and their employee ID. The appointment model holds information for the date/time, reason, status, vin, customer, and technician for an appointment. The automobileVO holds information for a vin number, which is grabbed from the Automobile model when polled. Vins from the AutomobileVO means that the car was purchased from CarCar, so if the vin number from the appointment model matches the vin number from automobile, it means the customer is a VIP.


## Sales microservice
- Created a Salesperson model that stores first and last name as well as employee id. This is later used to keep track of sales within the inventory microservice.

- Created a Customer model that stores first name, last name, address and phone number. This information is used when making and recording a sale within the inventory microservice.

- Created an AutomobileVO model that holds a unique vin attribute. This is important to keep track of unique automobile models within the inventory microservice.

- Created a Sale model with a price, automobile, salesperson and customer properties. All properties, with the exception of price, are ForeignKey's all related to sales. Crucial when recording a sale within the inventory microservice.

- When listing sales  (by salesperson), it interacts with the microservice by locating the car's unique VIN #.

- When recording a sale, the inventory microservice's automobile model is needed to interact with the Sale Form to record a sale.


## Inventory API Documentation:
**Automobile Information:**
**List Automobiles:**
http://localhost:8100/api/automobiles/
(GET request)
    Data Received:
```json
    {
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		}
	]
}
```


**Create Automobile:**
http://localhost:8100/api/automobiles/
(POST request)
    Format:
```json
        {
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
    }
```
**Get automobile detail**: http://localhost:8100/api/automobiles/:vin/ (GET request)
**Upate automobile detail**: http://localhost:8100/api/automobiles/:vin/ (PUT request)
**Delete automobile detail**: http://localhost:8100/api/automobiles/:vin/ (DELETE request)

**Vehicle Models:**
-List Models: http://localhost:8100/api/models/ (GET request)
    Data Received:
```json
    {
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```

**Create Model:**
http://localhost:8100/api/models/
(POST request)
    Format:
```json
        {
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

**Get model detail:** http://localhost:8100/api/models/:id/ (GET request)
**Update Model**: http://localhost:8100/api/models/:id/ (PUT request)
**Delete Model:** http://localhost:8100/api/models/:id/ (DELTE request)




**Manufacturers:**

**List Manufacturers:**
http://localhost:8100/api/manufacturers/
(GET request)
    Data Received:
```json
        {
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	]
}
```

**Create Manufacturer:**
http://localhost:8100/api/manufacturers/
(POST request)
```json
    Format: {
        "name": "Chrysler"
    }
```
**Get Manufacturer Detail:** http://localhost:8100/api/manufacturers/:id/ (GET request)

**Update Maufacturer:**
http://localhost:8100/api/manufacturers/:id/
(PUT request)
```json
    Format: {
        "name": "Chrysler"
    }
```

**Delete Manufacturer:** http://localhost:8100/api/manufacturers/:id/ (DELETE request)


## Sales API Documentation

**List Salespeople:**
http://localhost:8090/api/salespeople/
(GET request)

http://localhost:8090/api/salespeople/
(POST request)
```json
    Format: {
        "first_name": "Jane",
        "last_name": "Doe",
        "employee_id": 21
    }
```
**Delete Salesperson:** http://localhost:8090/api/salespeople/:id (DELETE request)
**List Customers:** http://localhost:8090/api/customers/ (GET request)
 http://localhost:8090/api/customers/ (POST request)
```json
    Format: {
        "first_name": "Jarf",
        "last_name": "Bayzeus",
        "address": "123 Real St., Beverly Hills, CA 90210",
        "phone_number": "310-555-1234"
    }
```
**Delete Customer:** http://localhost:8090/api/customers/:id (DELETE request)
**List Sales:** http://localhost:8090/api/sales/ (GET request)
**Create a Sale:**
http://localhost:8090/api/sales/
(POST request)
```json
    Format: {
        "salesperson": 10,
		"customer": 1,
		"automobile": "1C3CC5FB2AN120174",
		"price": 100
    }
```

Will give you a response that looks like:

```json
	Format: {
		"href": "/api/sales/3/",
		"price": 100,
		"automobile": {
			"import_href": "/api/automobiles/1C3CC5FB2AN120174/",
			"vin": "1C3CC5FB2AN120174"
		}
	}
```

**Delete a Sale:** http://localhost:8090/api/sales/:id (DELETE request)

## Service API Documentation

**GET TECHNICIANS**:
http://localhost:8080/api/technicians/
(GET request)
    Data Received:
```json
        {
	"technicians": [
		{
			"first_name": "mel",
			"last_name": "oberg",
			"employee_id": "moberg",
			"id": 4
		}
    ]}
```
**CREATE TECHNICIAN**:
http://localhost:8080/api/technicians/
(POST request)
    Format:
```json
        {
	"first_name": "Storm",
	"last_name": "Safstrom",
	"employee_id": "moberg"
}
```
**DELETE TECHNICIAN**:
http://localhost:8080/api/technicians/:id
(DELETE request)
    Data Recieved:
```json
    {
	"deleted": true
}
```



**GET APPOINTMENTS**:
http://localhost:8080/api/appointments/
(GET request)
    Data Received:
```json
        {
	"appointments": [
		{
			"date_time": "2023-05-01T10:00:00+00:00",
			"reason": "Oil Change",
			"status": "Scheduled",
			"vin": "1C3CC5FB2AN120174",
			"customer": "Blair",
			"technician": {
				"first_name": "Joe",
				"last_name": "Doe",
				"employee_id": "jdoe",
				"id": 10
			},
			"id": 5
		}
    ]
}
```

CREATE APPOINTMENT:
http://localhost:8080/api/appointments/
(POST request)
    Format:
```json
        {
            "date_time": "2023-03-22T21:30:22+00:00",
            "reason": "flat tire",
            "customer": "Melody",
            "technician": 4,
            "vin": "JH4DA1840KS004941"
            }
```
**DELETE APPOINTMENT:**
http://localhost:8080/api/appointments/:id
(DELETE request)
Data Recieved:
```json
{
	"deleted": true
}
```

**CANCEL APPOINTMENT:**
http://localhost:8080/api/appointments/:id/cancel
(PUT request)
Data Recieved:
```json
 {
	"date_time": "2023-05-01T10:00:00+00:00",
	"reason": "Oil Change",
	"status": "Cancelled",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Blair",
	"technician": {
		"first_name": "Joe",
		"last_name": "Doe",
		"employee_id": "jdoe",
		"id": 10
	},
	"id": 5
}
```
**FINISH APPOINTMENT**
http://localhost:8080/api/appointments/:id/finish
(PUT request)
Data Recieved:
```json
{
	"date_time": "2023-05-01T10:00:00+00:00",
	"reason": "Oil Change",
	"status": "Finished",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Blair",
	"technician": {
		"first_name": "Joe",
		"last_name": "Doe",
		"employee_id": "jdoe",
		"id": 10
	},
	"id": 5
}
```


## Value Objects

In CarCar, we apply the value object, AutomobileVO, which comes from the Automobile model in the Inventory microservice. The value object was created to grab the relevant information associated with an automobile. The AutomobileVO has one field for the Vehicle Identification Number (VIN), which allows us to retrieve the VIN number associated with a specific automobile.


## Diagram
Link: https://excalidraw.com/#room=b29272a717ec1b74fc94,q8eEoJvBtgR95DM5kNk0iQ
